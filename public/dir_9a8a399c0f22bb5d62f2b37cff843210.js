var dir_9a8a399c0f22bb5d62f2b37cff843210 =
[
    [ "GameObjectExtensions.cs", "_game_object_extensions_8cs.html", [
      [ "GameObjectExtensions", "class_tanks_1_1_extensions_1_1_game_object_extensions.html", "class_tanks_1_1_extensions_1_1_game_object_extensions" ]
    ] ],
    [ "IListExtensions.cs", "_i_list_extensions_8cs.html", "_i_list_extensions_8cs" ],
    [ "MathUtilities.cs", "_math_utilities_8cs.html", [
      [ "MathUtilities", "class_tanks_1_1_utilities_1_1_math_utilities.html", "class_tanks_1_1_utilities_1_1_math_utilities" ]
    ] ],
    [ "MobileDisable.cs", "_mobile_disable_8cs.html", [
      [ "MobileDisable", "class_tanks_1_1_utilities_1_1_mobile_disable.html", "class_tanks_1_1_utilities_1_1_mobile_disable" ]
    ] ],
    [ "MobileUtilities.cs", "_mobile_utilities_8cs.html", [
      [ "MobileUtilities", "class_tanks_1_1_utilities_1_1_mobile_utilities.html", "class_tanks_1_1_utilities_1_1_mobile_utilities" ]
    ] ],
    [ "PersistentSingleton.cs", "_persistent_singleton_8cs.html", [
      [ "PersistentSingleton", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", "class_tanks_1_1_utilities_1_1_persistent_singleton" ]
    ] ],
    [ "Singleton.cs", "_singleton_8cs.html", [
      [ "Singleton", "class_tanks_1_1_utilities_1_1_singleton.html", "class_tanks_1_1_utilities_1_1_singleton" ]
    ] ],
    [ "StringBuilding.cs", "_string_building_8cs.html", [
      [ "StringBuilding", "class_tanks_1_1_utilities_1_1_string_building.html", "class_tanks_1_1_utilities_1_1_string_building" ]
    ] ],
    [ "Vector2Extensions.cs", "_vector2_extensions_8cs.html", [
      [ "Vector2Extensions", "class_tanks_1_1_vector2_extensions.html", "class_tanks_1_1_vector2_extensions" ]
    ] ]
];