var struct_tanks_1_1_data_1_1_tank_type_definition =
[
    [ "armourRating", "struct_tanks_1_1_data_1_1_tank_type_definition.html#afa2e4ebb5435f38181a58f70125bceef", null ],
    [ "cost", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a9112f60e6aabc75eee43e94851c71ac8", null ],
    [ "description", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a14786a5149e70f545851e6d8b2b1861d", null ],
    [ "displayPrefab", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a5c105da907d1904b037c5cb52841ec6b", null ],
    [ "fireRate", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a5fae9d1c52c753c113b2b2326b2ef17f", null ],
    [ "hitPoints", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a33ce8dd877c3f5e75383e687d9cd6e21", null ],
    [ "id", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a89a9a7a5766a9f4400458730b3b7ad1a", null ],
    [ "name", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a20c1ae1d19030b3ebbbf3c9a367f40b7", null ],
    [ "refireRating", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a9d3e235c5c5281a37b953a89fb95256d", null ],
    [ "speed", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a0c54bc450a266efd5b599dbad0a12f3a", null ],
    [ "speedRating", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a566385e8b17dbdd1062d32249b0b7584", null ],
    [ "turnRate", "struct_tanks_1_1_data_1_1_tank_type_definition.html#a484abc97d320c34b91760b576818d567", null ]
];