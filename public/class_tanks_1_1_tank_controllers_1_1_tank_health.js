var class_tanks_1_1_tank_controllers_1_1_tank_health =
[
    [ "Damage", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#af6d6edc0eb71fadb38590e6ef7ae7b79", null ],
    [ "GetPosition", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ad34dcd0bb2906f24e327dba8503b2559", null ],
    [ "Init", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a4a1df09f8808718aa1e6caeba099b985", null ],
    [ "InternalOnZeroHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a858ffadcb74d4f48ff78f40e0136cf8c", null ],
    [ "IsPlayerDead", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ac4dab8d25470901d545d0bcef2683368", null ],
    [ "NullifySpawnPoint", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a356107fa8033bad3ac1bf6675e8ea606", null ],
    [ "OnCurrentHealthChanged", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a28fde823bec71af78d8eb54b9b91a0a3", null ],
    [ "OnShieldLevelChanged", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a42c3b8a5e2e930f3e8190cebbfaaffc4", null ],
    [ "OnZeroHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a772421c5d3959aa0ba69b738ff9bf3ab", null ],
    [ "RpcDamageFlash", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a171933e2dfc4e47c7c0b2683efe5337a", null ],
    [ "RpcDelayedReset", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a0fc9110ebb5268eb143f6904c6788b0d", null ],
    [ "RpcOnZeroHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a8ad605479d8a4ef9e4faba5c4b5f08b0", null ],
    [ "SetDamagedBy", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ab79a12ded60588fd8d38c281fb429ca6", null ],
    [ "SetDefaults", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a7914dcd04736a6454ddb86c23d63091a", null ],
    [ "SetShieldLevel", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a4157d9ea4d2c4bb19b20e14b3f697d39", null ],
    [ "SetTankActive", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ab902beb56fd1811025c03f64841e6f11", null ],
    [ "m_AimCanvas", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ab1127fc490cc1f52579d3a154159b3b5", null ],
    [ "m_Collider", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a2bc950950a1440c05d96f45e172c23e9", null ],
    [ "m_CurrentHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a547ed975958d6cd01ee3eddf44ce8b81", null ],
    [ "m_CurrentSpawnPoint", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a4b83b7d0d220a41e2d618774307288f4", null ],
    [ "m_DeathExplosion", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ac7eddbaa5e30109dcb4e50ee5726d479", null ],
    [ "m_LastDamagedByExplosionId", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a859f3e30f7ad944d98e2674e5bc65bb1", null ],
    [ "m_LastDamagedByPlayerNumber", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ad297821958d83f0220263582321832e4", null ],
    [ "m_Manager", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#afe13ac23f47c0951039070b7db457cde", null ],
    [ "m_ShieldLevel", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a62a6db87ef9c7c24d5ff1093539c363c", null ],
    [ "m_StartingHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#afb4d1409565b0942382397af1d26e06d", null ],
    [ "m_TankDisplay", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a90477b0e9a4c0116695bfc579fd0bca6", null ],
    [ "m_ZeroHealthHappened", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a8e45af48282222f2c7ef1a317afe4c28", null ],
    [ "TANK_SUICIDE_INDEX", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#af8464a89233812c2624b53270cacc2e1", null ],
    [ "currentSpawnPoint", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a69c4fabd3b4a2d3c496fb64e71b16875", null ],
    [ "invulnerable", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a5a3d1a37eca599e9e186356a673c4e89", null ],
    [ "isAlive", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ab046485671589b4ac72f4f330d4b4744", null ],
    [ "lastDamagedByExplosionId", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a6ba6857126c1771ccda53648735776f7", null ],
    [ "lastDamagedByPlayerNumber", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#af0f83be0e3f92e71465650dff3d4f4c7", null ],
    [ "healthChanged", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a290c5b4be4218c784cb1d86824405373", null ],
    [ "playerDeath", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ad731a11109b46741bca2259c7edd004f", null ],
    [ "playerReset", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#a2a2142e259f3b989275819c63b535cbc", null ],
    [ "shieldChanged", "class_tanks_1_1_tank_controllers_1_1_tank_health.html#ac035ae90db028c7f21d543ce3070d0cc", null ]
];