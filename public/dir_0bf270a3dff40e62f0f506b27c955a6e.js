var dir_0bf270a3dff40e62f0f506b27c955a6e =
[
    [ "CameraFollow.cs", "_camera_follow_8cs.html", [
      [ "CameraFollow", "class_tanks_1_1_camera_control_1_1_camera_follow.html", "class_tanks_1_1_camera_control_1_1_camera_follow" ]
    ] ],
    [ "CameraRigRotationInit.cs", "_camera_rig_rotation_init_8cs.html", [
      [ "CameraRigRotationInit", "class_tanks_1_1_camera_control_1_1_camera_rig_rotation_init.html", "class_tanks_1_1_camera_control_1_1_camera_rig_rotation_init" ]
    ] ],
    [ "CameraSnapshot.cs", "_camera_snapshot_8cs.html", [
      [ "CameraSnapshot", "class_tanks_1_1_camera_control_1_1_camera_snapshot.html", "class_tanks_1_1_camera_control_1_1_camera_snapshot" ]
    ] ],
    [ "ScreenShakeController.cs", "_screen_shake_controller_8cs.html", [
      [ "ScreenShakeController", "class_tanks_1_1_camera_control_1_1_screen_shake_controller.html", "class_tanks_1_1_camera_control_1_1_screen_shake_controller" ],
      [ "ShakeSettings", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_settings.html", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_settings" ],
      [ "ShakeInstance", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance" ]
    ] ]
];