var _main_menu_u_i_8cs =
[
    [ "MainMenuUI", "class_tanks_1_1_u_i_1_1_main_menu_u_i.html", "class_tanks_1_1_u_i_1_1_main_menu_u_i" ],
    [ "TanksNetworkPlayer", "_main_menu_u_i_8cs.html#aaad72235e919e98d601925ed8f2803e0", null ],
    [ "MenuPage", "_main_menu_u_i_8cs.html#a8f77e01d586dbcae9c0f280bfa23f9ed", [
      [ "Home", "_main_menu_u_i_8cs.html#a8f77e01d586dbcae9c0f280bfa23f9eda8cf04a9734132302f96da8e113e80ce5", null ],
      [ "SinglePlayer", "_main_menu_u_i_8cs.html#a8f77e01d586dbcae9c0f280bfa23f9edabaa1d5d7c2a65312fdc5577125de257a", null ],
      [ "Lobby", "_main_menu_u_i_8cs.html#a8f77e01d586dbcae9c0f280bfa23f9eda0d4c3d1b3f0c14e4aae9e9598ed745db", null ],
      [ "CustomizationPage", "_main_menu_u_i_8cs.html#a8f77e01d586dbcae9c0f280bfa23f9eda51bc99bb4fc27c6048b478fd22227ebc", null ]
    ] ]
];