var _network_manager_8cs =
[
    [ "NetworkManager", "class_tanks_1_1_networking_1_1_network_manager.html", "class_tanks_1_1_networking_1_1_network_manager" ],
    [ "NetworkGameType", "_network_manager_8cs.html#a9e0f3b0db598720d777e90fdf73eae5f", [
      [ "Matchmaking", "_network_manager_8cs.html#a9e0f3b0db598720d777e90fdf73eae5fa829645bba6da17148d7951791eab72e9", null ],
      [ "Direct", "_network_manager_8cs.html#a9e0f3b0db598720d777e90fdf73eae5fafd1dd0c603be8170f9eae0be9f2f6afb", null ],
      [ "Singleplayer", "_network_manager_8cs.html#a9e0f3b0db598720d777e90fdf73eae5fa1dcc626e56db5397bea841d584be1e46", null ]
    ] ],
    [ "NetworkState", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6", [
      [ "Inactive", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6a3cab03c00dbd11bc3569afa0748013f0", null ],
      [ "Pregame", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6ab323fc0a2e36b8e42d8c34cd11f87cbc", null ],
      [ "Connecting", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6ae321c53b354930ba96f0243e652df458", null ],
      [ "InLobby", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6a8d107da0fdffa879f15668804d40156d", null ],
      [ "InGame", "_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6a7f3d370e94c8b1fea09838572013d8ec", null ]
    ] ],
    [ "SceneChangeMode", "_network_manager_8cs.html#a56c39e6d7cd75c1a5a6fe58c6fd7a5c4", [
      [ "None", "_network_manager_8cs.html#a56c39e6d7cd75c1a5a6fe58c6fd7a5c4a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "Game", "_network_manager_8cs.html#a56c39e6d7cd75c1a5a6fe58c6fd7a5c4a63d72051e901c069f8aa1b32aa0c43bb", null ],
      [ "Menu", "_network_manager_8cs.html#a56c39e6d7cd75c1a5a6fe58c6fd7a5c4ab61541208db7fa7dba42c85224405911", null ]
    ] ]
];