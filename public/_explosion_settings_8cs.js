var _explosion_settings_8cs =
[
    [ "ExplosionSettings", "class_tanks_1_1_explosions_1_1_explosion_settings.html", "class_tanks_1_1_explosions_1_1_explosion_settings" ],
    [ "ExplosionClass", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51", [
      [ "Large", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51a3a69b34ce86dacb205936a8094f6c743", null ],
      [ "Small", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51a2660064e68655415da2628c2ae2f7592", null ],
      [ "ExtraLarge", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51a008bcff19f8ff51e880a193fdb72fa76", null ],
      [ "TankExplosion", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51ad131a3877cb707e09cbc514508570a26", null ],
      [ "TurretExplosion", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51ad74a63c0af4f170e3928687555769347", null ],
      [ "BounceExplosion", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51ab174038c315d76170f2c5bf30f14fe7a", null ],
      [ "ClusterExplosion", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51ac4dcc1bd80e934f24afff32580e29ff8", null ],
      [ "FiringExplosion", "_explosion_settings_8cs.html#ad793b9c7a3609cbf12a65779f4afbc51a9bfab11af63e512f4baa1c8a7d8dd326", null ]
    ] ]
];