var dir_aa3b7ab08f4c107d50e2677e19a5d9e6 =
[
    [ "DailyUnlockManager.cs", "_daily_unlock_manager_8cs.html", [
      [ "DailyUnlockManager", "class_tanks_1_1_data_1_1_daily_unlock_manager.html", "class_tanks_1_1_data_1_1_daily_unlock_manager" ]
    ] ],
    [ "DataStore.cs", "_data_store_8cs.html", [
      [ "DecorationData", "class_tanks_1_1_data_1_1_decoration_data.html", "class_tanks_1_1_data_1_1_decoration_data" ],
      [ "LevelData", "class_tanks_1_1_data_1_1_level_data.html", "class_tanks_1_1_data_1_1_level_data" ],
      [ "SettingsData", "class_tanks_1_1_data_1_1_settings_data.html", "class_tanks_1_1_data_1_1_settings_data" ],
      [ "DataStore", "class_tanks_1_1_data_1_1_data_store.html", "class_tanks_1_1_data_1_1_data_store" ]
    ] ],
    [ "EncryptedJsonSaver.cs", "_encrypted_json_saver_8cs.html", [
      [ "EncryptedJsonSaver", "class_tanks_1_1_data_1_1_encrypted_json_saver.html", "class_tanks_1_1_data_1_1_encrypted_json_saver" ]
    ] ],
    [ "IDataSaver.cs", "_i_data_saver_8cs.html", [
      [ "IDataSaver", "interface_tanks_1_1_data_1_1_i_data_saver.html", "interface_tanks_1_1_data_1_1_i_data_saver" ]
    ] ],
    [ "JsonSaver.cs", "_json_saver_8cs.html", [
      [ "JsonSaver", "class_tanks_1_1_data_1_1_json_saver.html", "class_tanks_1_1_data_1_1_json_saver" ]
    ] ],
    [ "PlayerDataManager.cs", "_player_data_manager_8cs.html", [
      [ "PlayerDataManager", "class_tanks_1_1_data_1_1_player_data_manager.html", "class_tanks_1_1_data_1_1_player_data_manager" ]
    ] ],
    [ "SpecialProjectileLibrary.cs", "_special_projectile_library_8cs.html", "_special_projectile_library_8cs" ],
    [ "TankDecorationLibrary.cs", "_tank_decoration_library_8cs.html", [
      [ "TankDecorationDefinition", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html", "struct_tanks_1_1_data_1_1_tank_decoration_definition" ],
      [ "TankDecorationLibrary", "class_tanks_1_1_data_1_1_tank_decoration_library.html", "class_tanks_1_1_data_1_1_tank_decoration_library" ]
    ] ],
    [ "TankLibrary.cs", "_tank_library_8cs.html", [
      [ "TankTypeDefinition", "struct_tanks_1_1_data_1_1_tank_type_definition.html", "struct_tanks_1_1_data_1_1_tank_type_definition" ],
      [ "TankLibrary", "class_tanks_1_1_data_1_1_tank_library.html", "class_tanks_1_1_data_1_1_tank_library" ]
    ] ]
];