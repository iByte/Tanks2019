var namespace_tanks_1_1_rules =
[
    [ "SinglePlayer", "namespace_tanks_1_1_rules_1_1_single_player.html", "namespace_tanks_1_1_rules_1_1_single_player" ],
    [ "FreeForAll", "class_tanks_1_1_rules_1_1_free_for_all.html", "class_tanks_1_1_rules_1_1_free_for_all" ],
    [ "KillLogPhrases", "class_tanks_1_1_rules_1_1_kill_log_phrases.html", "class_tanks_1_1_rules_1_1_kill_log_phrases" ],
    [ "LastManStanding", "class_tanks_1_1_rules_1_1_last_man_standing.html", "class_tanks_1_1_rules_1_1_last_man_standing" ],
    [ "ModeDetails", "class_tanks_1_1_rules_1_1_mode_details.html", "class_tanks_1_1_rules_1_1_mode_details" ],
    [ "ModeList", "class_tanks_1_1_rules_1_1_mode_list.html", "class_tanks_1_1_rules_1_1_mode_list" ],
    [ "RulesProcessor", "class_tanks_1_1_rules_1_1_rules_processor.html", "class_tanks_1_1_rules_1_1_rules_processor" ],
    [ "Team", "class_tanks_1_1_rules_1_1_team.html", "class_tanks_1_1_rules_1_1_team" ],
    [ "TeamDeathmatch", "class_tanks_1_1_rules_1_1_team_deathmatch.html", "class_tanks_1_1_rules_1_1_team_deathmatch" ]
];