var struct_tanks_1_1_effects_1_1_effects_group =
[
    [ "bouncyBombExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#a704efdabb76332a448123ee864870acc", null ],
    [ "extraLargeExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#ac9745403f7ecea48573ca81aa41df2ee", null ],
    [ "extraLargeExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#a8dff660ea662fe4d2b60d289dd7d0775", null ],
    [ "firingExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#a45a3d7b517481456d559f7f901642dfc", null ],
    [ "group", "struct_tanks_1_1_effects_1_1_effects_group.html#a4e5a4ce36f18f129ec934c97ea96edbf", null ],
    [ "largeExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#acc34594cde13909f88ef0e04fa6dbd44", null ],
    [ "largeExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#aae2ad45aa8d21720dd2785aa2e3a8185", null ],
    [ "smallExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#a94a7c27bf7750af4e73639d886042ece", null ],
    [ "smallExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#acfe9aae8672f3e32204f9d7e90dad50f", null ],
    [ "tankExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#a384935081af7976c1c96c1f293b3d956", null ],
    [ "tankExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#a57b936374db83d25b251f794db1088bf", null ],
    [ "tankTrackParticles", "struct_tanks_1_1_effects_1_1_effects_group.html#a06f2f2743d456430414dc62d48911dbb", null ],
    [ "turretExplosion", "struct_tanks_1_1_effects_1_1_effects_group.html#a7ef3ac1061876120a300f8ed9a95a6cd", null ],
    [ "turretExplosionSounds", "struct_tanks_1_1_effects_1_1_effects_group.html#a78be3700161216e495373f1b2463aeb3", null ]
];