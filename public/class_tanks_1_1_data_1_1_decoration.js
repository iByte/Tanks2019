var class_tanks_1_1_data_1_1_decoration =
[
    [ "Attach", "class_tanks_1_1_data_1_1_decoration.html#a3847e83c0f7701a46bb5d00345a9867c", null ],
    [ "Detach", "class_tanks_1_1_data_1_1_decoration.html#ad5d3a46145344a2bc29769dd05fc0742", null ],
    [ "GetDecorationBounds", "class_tanks_1_1_data_1_1_decoration.html#ab6099e0bbc8359609bff92586c307bf4", null ],
    [ "SetMaterial", "class_tanks_1_1_data_1_1_decoration.html#ae00b226511cd84661f8a571c07af4c40", null ],
    [ "m_DeathLayer", "class_tanks_1_1_data_1_1_decoration.html#a5099bcd5582ccbf5134e71105a30b83e", null ],
    [ "m_DecorationBaseRenderers", "class_tanks_1_1_data_1_1_decoration.html#a717560ce77fa5e41cc5c5fb6281bd8b3", null ],
    [ "m_DetachedLifetime", "class_tanks_1_1_data_1_1_decoration.html#a3c73963f88fff67b8e4caa4624af6e4d", null ],
    [ "m_JointsToDestroy", "class_tanks_1_1_data_1_1_decoration.html#abe84edcec0f614e897e2c3f0fa22e5c2", null ],
    [ "m_TankRef", "class_tanks_1_1_data_1_1_decoration.html#a6cc198c0b7de26403e77a8ffd7058010", null ]
];