var _game_manager_8cs =
[
    [ "GameManager", "class_tanks_1_1_game_manager.html", "class_tanks_1_1_game_manager" ],
    [ "TanksNetworkManager", "_game_manager_8cs.html#a1d4f47193767fbe5234287202ccd340f", null ],
    [ "TanksNetworkPlayer", "_game_manager_8cs.html#aaad72235e919e98d601925ed8f2803e0", null ],
    [ "GameState", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845ed", [
      [ "Inactive", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845eda3cab03c00dbd11bc3569afa0748013f0", null ],
      [ "TimedTransition", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edaede693a87b5d286ef8a7bda542352b79", null ],
      [ "StartUp", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845eda62697cf4b8d5f13a548430b5be1be665", null ],
      [ "Preplay", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845eda2b93fab1927276661678a21e05617953", null ],
      [ "Preround", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edaaf37d412c0297fea36dd5e51a90f930d", null ],
      [ "Playing", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edac9dbb2b7c84159b632d71e512eba8428", null ],
      [ "RoundEnd", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845eda85f2151e51ce5218e9eccdf9e421a824", null ],
      [ "EndGame", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edaf0f29c4900d44c9e32d203ca799163e1", null ],
      [ "PostGame", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edae5b456c484a0ec6a32301d03121fd80b", null ],
      [ "EveryoneBailed", "_game_manager_8cs.html#a47ddd5a1c10166f7fef3e78ecb3845edabbccb1c3bfab62176bfb4237ebbb381d", null ]
    ] ]
];