var class_tanks_1_1_u_i_1_1_team_color_provider =
[
    [ "CanUseColor", "class_tanks_1_1_u_i_1_1_team_color_provider.html#a7a3c6a72cbca7d13dd6eca5fb48fc637", null ],
    [ "Reset", "class_tanks_1_1_u_i_1_1_team_color_provider.html#a84aa5d06a72794ae0c73bc94c2fc8281", null ],
    [ "ServerGetColor", "class_tanks_1_1_u_i_1_1_team_color_provider.html#aff6f38d2030212eb6e0c2490e6482938", null ],
    [ "SetupColors", "class_tanks_1_1_u_i_1_1_team_color_provider.html#a7f7a8352fb68ce862299e6b5a2198cff", null ],
    [ "m_Colors", "class_tanks_1_1_u_i_1_1_team_color_provider.html#a001780f88575b1353693d1d61f3e24c3", null ],
    [ "m_LastUsedColorIndex", "class_tanks_1_1_u_i_1_1_team_color_provider.html#abca6505a166571b0f57faf272150e0db", null ],
    [ "m_Players", "class_tanks_1_1_u_i_1_1_team_color_provider.html#a85f6c8a49a96676c80943f6706bd5a3f", null ]
];