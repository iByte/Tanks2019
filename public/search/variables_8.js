var searchData=
[
  ['id_3377',['id',['../class_tanks_1_1_data_1_1_level_data.html#afc67199efca2ca55158b825f8363fd62',1,'Tanks.Data.LevelData.id()'],['../struct_tanks_1_1_data_1_1_projectile_definition.html#a6df31c401c8ca631c31c2cd74463ccbd',1,'Tanks.Data.ProjectileDefinition.id()'],['../struct_tanks_1_1_data_1_1_tank_decoration_definition.html#aa52eb1de3a8e202b62bb76828a141de2',1,'Tanks.Data.TankDecorationDefinition.id()'],['../struct_tanks_1_1_data_1_1_tank_type_definition.html#a89a9a7a5766a9f4400458730b3b7ad1a',1,'Tanks.Data.TankTypeDefinition.id()'],['../class_tanks_1_1_explosions_1_1_explosion_settings.html#a7c26b6acf0e88ff81799575f1156c8c6',1,'Tanks.Explosions.ExplosionSettings.id()']]],
  ['isleftymode_3378',['isLeftyMode',['../class_tanks_1_1_data_1_1_settings_data.html#aa1268b5ca3a3b9be6e1aaa8f47537d9b',1,'Tanks::Data::SettingsData']]]
];
