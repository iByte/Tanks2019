var searchData=
[
  ['physicsforce_4202',['physicsForce',['../class_tanks_1_1_explosions_1_1_explosion_settings.html#a4d8a6a12d7149f09dcefe1d6a9a5d73a',1,'Tanks::Explosions::ExplosionSettings']]],
  ['physicsradius_4203',['physicsRadius',['../class_tanks_1_1_explosions_1_1_explosion_settings.html#a195143a9ef04b01cdbf926fb46ff96f1',1,'Tanks::Explosions::ExplosionSettings']]],
  ['pickupname_4204',['pickupName',['../class_tanks_1_1_single_player_1_1_collectible.html#ab329d50297f779d6e1a8979036639c2a',1,'Tanks::SinglePlayer::Collectible']]],
  ['playername_4205',['playerName',['../class_tanks_1_1_data_1_1_data_store.html#ac593713a478e7cf05f11f9c316514896',1,'Tanks::Data::DataStore']]],
  ['powerupprefab_4206',['powerupPrefab',['../struct_tanks_1_1_pickups_1_1_powerup_definition.html#ab6c02da1ad25fc95e78547ac08542e40',1,'Tanks::Pickups::PowerupDefinition']]],
  ['prefab_4207',['prefab',['../class_tanks_1_1_explosions_1_1_debris_settings.html#a8405a43b416cf6e1863814661276513f',1,'Tanks::Explosions::DebrisSettings']]],
  ['preview_4208',['preview',['../struct_tanks_1_1_data_1_1_tank_decoration_definition.html#a4e518a01a079a60c93c4c9fb35bcc912',1,'Tanks::Data::TankDecorationDefinition']]],
  ['projectileprefab_4209',['projectilePrefab',['../struct_tanks_1_1_data_1_1_projectile_definition.html#a939180a9ea516360c13f2d55449e3708',1,'Tanks::Data::ProjectileDefinition']]]
];
