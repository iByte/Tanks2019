var searchData=
[
  ['name_4199',['name',['../struct_tanks_1_1_data_1_1_projectile_definition.html#a705584de720777eacbf80d550e7f1ce2',1,'Tanks.Data.ProjectileDefinition.name()'],['../struct_tanks_1_1_data_1_1_tank_decoration_definition.html#aa49cc5770c437b793d3069e1cc7ebad0',1,'Tanks.Data.TankDecorationDefinition.name()'],['../struct_tanks_1_1_data_1_1_tank_type_definition.html#a20c1ae1d19030b3ebbbf3c9a367f40b7',1,'Tanks.Data.TankTypeDefinition.name()'],['../struct_tanks_1_1_pickups_1_1_powerup_definition.html#a096717d568f28fd3208944c1e4105027',1,'Tanks.Pickups.PowerupDefinition.name()']]],
  ['notificationprefab_4200',['notificationPrefab',['../class_tanks_1_1_u_i_1_1_in_game_notification_manager.html#a08d8c78e8c1fc9ca4adea3fea64fafa0',1,'Tanks::UI::InGameNotificationManager']]]
];
