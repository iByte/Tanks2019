var class_tanks_1_1_data_1_1_data_store =
[
    [ "DataStore", "class_tanks_1_1_data_1_1_data_store.html#af817947c06ffcc232bd0064fd681f619", null ],
    [ "GetAllLevelData", "class_tanks_1_1_data_1_1_data_store.html#abe1d97f16e49434951427fe1e9f39d64", null ],
    [ "GetLevelData", "class_tanks_1_1_data_1_1_data_store.html#a81f6ff35eb6cea1ecd2f0ba25f3a3fe3", null ],
    [ "LevelDataDeserialize", "class_tanks_1_1_data_1_1_data_store.html#a5216ad20e1a37e72a32ff2a01cc526d3", null ],
    [ "LevelDataSerialize", "class_tanks_1_1_data_1_1_data_store.html#aea22f607b7eb5efac8ec9f674bc54cc6", null ],
    [ "OnAfterDeserialize", "class_tanks_1_1_data_1_1_data_store.html#a21aadb8d93e7105ffc70501fe5f415e9", null ],
    [ "OnBeforeSerialize", "class_tanks_1_1_data_1_1_data_store.html#a9f0bd5014a7673de6e2f9faa55d32051", null ],
    [ "currency", "class_tanks_1_1_data_1_1_data_store.html#aee26d9884b08736d23e0b414ca7e68df", null ],
    [ "decorations", "class_tanks_1_1_data_1_1_data_store.html#af95f7677e25719b8ea97e61359444585", null ],
    [ "levels", "class_tanks_1_1_data_1_1_data_store.html#a915b6bbd031b45e0fcbf6659722df255", null ],
    [ "m_LevelsDictionary", "class_tanks_1_1_data_1_1_data_store.html#a122820b894c2101dec92f2260aa85735", null ],
    [ "playerName", "class_tanks_1_1_data_1_1_data_store.html#ac593713a478e7cf05f11f9c316514896", null ],
    [ "s_DefaultName", "class_tanks_1_1_data_1_1_data_store.html#a1340b845aa60c027bcd750ed75dc7408", null ],
    [ "selectedDecoration", "class_tanks_1_1_data_1_1_data_store.html#a8cf33e08d47e75763a68a0abe3b7cbd3", null ],
    [ "selectedTank", "class_tanks_1_1_data_1_1_data_store.html#a2a6fd0caf75fd6fc0079a3787745296e", null ],
    [ "settingsData", "class_tanks_1_1_data_1_1_data_store.html#abf5ebbf451eb938d4ba0ec3bec8e6075", null ],
    [ "tempUnlockColour", "class_tanks_1_1_data_1_1_data_store.html#aacd79c5d3c9b15a76417d9c0e3804dd4", null ],
    [ "tempUnlockDate", "class_tanks_1_1_data_1_1_data_store.html#a961623b717d068a8c00978ff73cf454d", null ],
    [ "tempUnlockId", "class_tanks_1_1_data_1_1_data_store.html#a63f6d3facb835d5c74365cc959ee6928", null ],
    [ "unlockedMultiplayerMaps", "class_tanks_1_1_data_1_1_data_store.html#a01ed0b79aefd8ca22d4eb35b3d0ca21a", null ],
    [ "unlockedTanks", "class_tanks_1_1_data_1_1_data_store.html#a3ffeee50b4a0267c499a68de521488ae", null ]
];