var class_tanks_1_1_data_1_1_settings_data =
[
    [ "everyplayEnabled", "class_tanks_1_1_data_1_1_settings_data.html#a85295e1cd16fa85c7921695438307edd", null ],
    [ "isLeftyMode", "class_tanks_1_1_data_1_1_settings_data.html#aa1268b5ca3a3b9be6e1aaa8f47537d9b", null ],
    [ "masterVolume", "class_tanks_1_1_data_1_1_settings_data.html#a0079ed0e717bf80c5ac9d3d7c820fdc1", null ],
    [ "musicVolume", "class_tanks_1_1_data_1_1_settings_data.html#ababbb792e15a34496872ea601c2988f5", null ],
    [ "sfxVolume", "class_tanks_1_1_data_1_1_settings_data.html#a7e5db1ddea949624a0a06e38015255dc", null ],
    [ "thumbstickSize", "class_tanks_1_1_data_1_1_settings_data.html#a435f7647d9059545ec1b33f2b9b22f5d", null ]
];