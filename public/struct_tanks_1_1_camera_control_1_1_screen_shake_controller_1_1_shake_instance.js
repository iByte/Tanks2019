var struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance =
[
    [ "StopShake", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#af50252a439f396858af81b8d6998188e", null ],
    [ "direction", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a914d94593c3db0cba0d1fd3d39f92a9a", null ],
    [ "duration", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a7ef6b3afda5fe22cd7ec4422cae4ae13", null ],
    [ "magnitude", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a46b6e71da60c5099f1647332e4eda1b4", null ],
    [ "maxDuration", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a18a4cc1a0e967ed4dc09e57258ab4160", null ],
    [ "shakeId", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a61003d7bc52e9c48da5ddfcc6bd84b62", null ],
    [ "done", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a555672a1a0e59f47a89b11c9ef392aba", null ],
    [ "normalizedProgress", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html#a0530fdc2ee6fa3097d29d43d5dec0cf2", null ]
];