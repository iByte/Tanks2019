/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Tanks 2019", "index.html", [
    [ "Tanks!!! 2019 Documentation", "index.html", [
      [ "Introduction", "index.html#autotoc_md0", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Typedefs", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_advert_unlock_modal_8cs.html",
"_network_manager_8cs.html#a06594d26b587a2d419599701a68d7ff6a3cab03c00dbd11bc3569afa0748013f0",
"class_tanks_1_1_audio_1_1_music_manager.html#af000844a856fcc31096c4334801aa2d7",
"class_tanks_1_1_explosions_1_1_explosion_manager.html#ad90e5b4e88c2c44d92f2a39377617eaf",
"class_tanks_1_1_map_1_1_map_details.html#a860ca07f9911740f5edb52b8ad3f6d18",
"class_tanks_1_1_pickups_1_1_tank_seeker.html#afde74aa683c34d539bc58b70e6971bb1",
"class_tanks_1_1_rules_1_1_single_player_1_1_shooting_range_rules_processor.html#a3489279febe8e8dbae4e107eef9c182e",
"class_tanks_1_1_tank_controllers_1_1_tank_health.html#ad297821958d83f0220263582321832e4",
"class_tanks_1_1_u_i_1_1_android_back_quit.html#a296ed9b099e3ffb42a8b78a93442e199",
"class_tanks_1_1_u_i_1_1_leaderboard_element.html#ab89e6fbc8f4a7e8c9b614d9b309a9fab",
"class_tanks_1_1_u_i_1_1_map_select.html#a8ab416f717920f9497735f4697ef1b28",
"class_tanks_1_1_u_i_1_1_single_player_start_game_modal.html#a3a11127714009fa228d9a522caf65446",
"functions_i.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';