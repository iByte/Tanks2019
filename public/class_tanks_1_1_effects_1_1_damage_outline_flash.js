var class_tanks_1_1_effects_1_1_damage_outline_flash =
[
    [ "Awake", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a1197c0c6b8bf59446faae6cb35c3da31", null ],
    [ "StartDamageFlash", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a6eda1b8b3abcd90e264fdcf31c30d082", null ],
    [ "Update", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a07ec8334c1708dac82420b68b42646e1", null ],
    [ "m_BorderBaseMaterial", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a70df990b912ac392e7ef2781728130b7", null ],
    [ "m_BorderBaseThickness", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a49815e3d984634b93b8ec9896d83a99f", null ],
    [ "m_BorderRenderers", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a758709d9f806b86a9669c689d1f6be13", null ],
    [ "m_DamageBorderPulseAmount", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a0f160b035681ac3a63d16dedc9f7ca4a", null ],
    [ "m_DamageColor", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a1aef0a61f3cd2ca6d26593ff7531fadc", null ],
    [ "m_DamageFadeDuration", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#a22d54912abc398b7602679bbec144fe0", null ],
    [ "m_DamageFadeTime", "class_tanks_1_1_effects_1_1_damage_outline_flash.html#abbaa0360f7c1f82da72f0f033fa432bd", null ]
];