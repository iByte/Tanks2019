var struct_tanks_1_1_data_1_1_tank_decoration_definition =
[
    [ "availableMaterials", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#a4831f631299e4369a569cf62fb1fbd65", null ],
    [ "crateDecal", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#af6e52ad693ae3f1bb41ff3c72a887dc3", null ],
    [ "decorationPrefab", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#a2ce9e3184e5cc72e043c1b111c36b261", null ],
    [ "id", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#aa52eb1de3a8e202b62bb76828a141de2", null ],
    [ "name", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#aa49cc5770c437b793d3069e1cc7ebad0", null ],
    [ "preview", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#a4e518a01a079a60c93c4c9fb35bcc912", null ],
    [ "selectionWeighting", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html#a205607e6ccad67f43e4ddd6428fe22db", null ]
];