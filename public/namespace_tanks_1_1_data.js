var namespace_tanks_1_1_data =
[
    [ "DailyUnlockManager", "class_tanks_1_1_data_1_1_daily_unlock_manager.html", "class_tanks_1_1_data_1_1_daily_unlock_manager" ],
    [ "DataStore", "class_tanks_1_1_data_1_1_data_store.html", "class_tanks_1_1_data_1_1_data_store" ],
    [ "Decoration", "class_tanks_1_1_data_1_1_decoration.html", "class_tanks_1_1_data_1_1_decoration" ],
    [ "DecorationData", "class_tanks_1_1_data_1_1_decoration_data.html", "class_tanks_1_1_data_1_1_decoration_data" ],
    [ "EncryptedJsonSaver", "class_tanks_1_1_data_1_1_encrypted_json_saver.html", "class_tanks_1_1_data_1_1_encrypted_json_saver" ],
    [ "IDataSaver", "interface_tanks_1_1_data_1_1_i_data_saver.html", "interface_tanks_1_1_data_1_1_i_data_saver" ],
    [ "JsonSaver", "class_tanks_1_1_data_1_1_json_saver.html", "class_tanks_1_1_data_1_1_json_saver" ],
    [ "LevelData", "class_tanks_1_1_data_1_1_level_data.html", "class_tanks_1_1_data_1_1_level_data" ],
    [ "PlayerDataManager", "class_tanks_1_1_data_1_1_player_data_manager.html", "class_tanks_1_1_data_1_1_player_data_manager" ],
    [ "ProjectileDefinition", "struct_tanks_1_1_data_1_1_projectile_definition.html", "struct_tanks_1_1_data_1_1_projectile_definition" ],
    [ "SettingsData", "class_tanks_1_1_data_1_1_settings_data.html", "class_tanks_1_1_data_1_1_settings_data" ],
    [ "SpecialProjectileLibrary", "class_tanks_1_1_data_1_1_special_projectile_library.html", "class_tanks_1_1_data_1_1_special_projectile_library" ],
    [ "TankDecorationDefinition", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html", "struct_tanks_1_1_data_1_1_tank_decoration_definition" ],
    [ "TankDecorationLibrary", "class_tanks_1_1_data_1_1_tank_decoration_library.html", "class_tanks_1_1_data_1_1_tank_decoration_library" ],
    [ "TankLibrary", "class_tanks_1_1_data_1_1_tank_library.html", "class_tanks_1_1_data_1_1_tank_library" ],
    [ "TankTypeDefinition", "struct_tanks_1_1_data_1_1_tank_type_definition.html", "struct_tanks_1_1_data_1_1_tank_type_definition" ]
];