var class_tanks_1_1_explosions_1_1_debris_settings =
[
    [ "maxForce", "class_tanks_1_1_explosions_1_1_debris_settings.html#afd99dde4a20232d7686f0b1922b64e05", null ],
    [ "maxSpawns", "class_tanks_1_1_explosions_1_1_debris_settings.html#a641b7dbdf43e86848967035278d13d2e", null ],
    [ "maxUpAngle", "class_tanks_1_1_explosions_1_1_debris_settings.html#a3805fd8582d561553c296e7bc65a5333", null ],
    [ "minForce", "class_tanks_1_1_explosions_1_1_debris_settings.html#a8af47e2ba5508d4eb2b4af98cae17f65", null ],
    [ "minSpawns", "class_tanks_1_1_explosions_1_1_debris_settings.html#ab128fcebcc8e5dadb522524c8b32a826", null ],
    [ "prefab", "class_tanks_1_1_explosions_1_1_debris_settings.html#a8405a43b416cf6e1863814661276513f", null ]
];