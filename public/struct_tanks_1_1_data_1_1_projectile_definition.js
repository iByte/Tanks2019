var struct_tanks_1_1_data_1_1_projectile_definition =
[
    [ "fireSound", "struct_tanks_1_1_data_1_1_projectile_definition.html#a86b220b6e2a8c0518ab6a519ecfb95be", null ],
    [ "id", "struct_tanks_1_1_data_1_1_projectile_definition.html#a6df31c401c8ca631c31c2cd74463ccbd", null ],
    [ "name", "struct_tanks_1_1_data_1_1_projectile_definition.html#a705584de720777eacbf80d550e7f1ce2", null ],
    [ "projectilePrefab", "struct_tanks_1_1_data_1_1_projectile_definition.html#a939180a9ea516360c13f2d55449e3708", null ],
    [ "weaponColor", "struct_tanks_1_1_data_1_1_projectile_definition.html#ae9d821ca80c104169a170fb2a62efd18", null ],
    [ "weaponIcon", "struct_tanks_1_1_data_1_1_projectile_definition.html#aa22afcc9b47b68179cf0066a50241ced", null ]
];