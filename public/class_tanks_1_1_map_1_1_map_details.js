var class_tanks_1_1_map_1_1_map_details =
[
    [ "m_Description", "class_tanks_1_1_map_1_1_map_details.html#a48a543ca024d170dd2bee7fdfd1c2c8e", null ],
    [ "m_EffectsGroup", "class_tanks_1_1_map_1_1_map_details.html#ac2a96e7cbcb336e3a72e1ee1ac4ee08e", null ],
    [ "m_Id", "class_tanks_1_1_map_1_1_map_details.html#aab063aff9c94ac44eaf06d29126f7f8b", null ],
    [ "m_Image", "class_tanks_1_1_map_1_1_map_details.html#a255ea62ded0f80e0544048340deb3a70", null ],
    [ "m_IsLocked", "class_tanks_1_1_map_1_1_map_details.html#a860ca07f9911740f5edb52b8ad3f6d18", null ],
    [ "m_LevelMusic", "class_tanks_1_1_map_1_1_map_details.html#ab11593d9c361a721aa465f814471d538", null ],
    [ "m_Name", "class_tanks_1_1_map_1_1_map_details.html#aa5e1fcaa787a99fc013b22b52908fcda", null ],
    [ "m_SceneName", "class_tanks_1_1_map_1_1_map_details.html#a8eb55876f0ccad98fe1e7b134dd97311", null ],
    [ "m_UnlockCost", "class_tanks_1_1_map_1_1_map_details.html#a60ae7c92d5f55a00ed79037c31b001ac", null ],
    [ "description", "class_tanks_1_1_map_1_1_map_details.html#abf21feaf6ab3d36edd9e120122917197", null ],
    [ "effectsGroup", "class_tanks_1_1_map_1_1_map_details.html#a0db91ea7d5bc3df161f9917547904cde", null ],
    [ "id", "class_tanks_1_1_map_1_1_map_details.html#a8adc814f2effaddd11dcc8e1fef07aac", null ],
    [ "image", "class_tanks_1_1_map_1_1_map_details.html#a6e9609d1975655b5c5add936b4dcba8f", null ],
    [ "isLocked", "class_tanks_1_1_map_1_1_map_details.html#a77a59840cc95f7f5a76edb711fa7c61f", null ],
    [ "levelMusic", "class_tanks_1_1_map_1_1_map_details.html#a9049fad1a7ab9e544cef894b88a0578a", null ],
    [ "name", "class_tanks_1_1_map_1_1_map_details.html#a72c6ce2c36a25297fdd672ceaf64427a", null ],
    [ "sceneName", "class_tanks_1_1_map_1_1_map_details.html#aa33d968a05c6458021d1dc3e23f765b5", null ],
    [ "unlockCost", "class_tanks_1_1_map_1_1_map_details.html#a777a8c264be05527f610db7ff72f2fe7", null ]
];