var hierarchy =
[
    [ "Tanks.Analytics.AnalyticsHelper", "class_tanks_1_1_analytics_1_1_analytics_helper.html", null ],
    [ "Tanks.Data.DecorationData", "class_tanks_1_1_data_1_1_decoration_data.html", null ],
    [ "Tanks.Effects.EffectsGroup", "struct_tanks_1_1_effects_1_1_effects_group.html", null ],
    [ "Tanks.Shells.FiringLogic", "class_tanks_1_1_shells_1_1_firing_logic.html", null ],
    [ "Tanks.Extensions.GameObjectExtensions", "class_tanks_1_1_extensions_1_1_game_object_extensions.html", null ],
    [ "Tanks.Analytics.HeatmapsHelper", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html", null ],
    [ "IBeginDragHandler", null, [
      [ "Tanks.UI.TankDragPreview", "class_tanks_1_1_u_i_1_1_tank_drag_preview.html", null ]
    ] ],
    [ "Tanks.UI.IColorProvider", "interface_tanks_1_1_u_i_1_1_i_color_provider.html", [
      [ "Tanks.UI.PlayerColorProvider", "class_tanks_1_1_u_i_1_1_player_color_provider.html", null ],
      [ "Tanks.UI.TeamColorProvider", "class_tanks_1_1_u_i_1_1_team_color_provider.html", null ]
    ] ],
    [ "Tanks.TankControllers.IDamageObject", "interface_tanks_1_1_tank_controllers_1_1_i_damage_object.html", [
      [ "Tanks.Hazards.MineController", "class_tanks_1_1_hazards_1_1_mine_controller.html", null ],
      [ "Tanks.Pickups.PickupBase", "class_tanks_1_1_pickups_1_1_pickup_base.html", [
        [ "Tanks.Pickups.CurrencyPickup", "class_tanks_1_1_pickups_1_1_currency_pickup.html", null ],
        [ "Tanks.Pickups.NitroPickup", "class_tanks_1_1_pickups_1_1_nitro_pickup.html", null ],
        [ "Tanks.Pickups.ProjectilePickup", "class_tanks_1_1_pickups_1_1_projectile_pickup.html", null ],
        [ "Tanks.Pickups.ShieldPickup", "class_tanks_1_1_pickups_1_1_shield_pickup.html", null ]
      ] ],
      [ "Tanks.SinglePlayer.Npc", "class_tanks_1_1_single_player_1_1_npc.html", [
        [ "Tanks.SinglePlayer.Crate", "class_tanks_1_1_single_player_1_1_crate.html", null ],
        [ "Tanks.SinglePlayer.TargetNpc", "class_tanks_1_1_single_player_1_1_target_npc.html", null ]
      ] ],
      [ "Tanks.TankControllers.TankHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html", null ]
    ] ],
    [ "Tanks.Data.IDataSaver", "interface_tanks_1_1_data_1_1_i_data_saver.html", [
      [ "Tanks.Data.JsonSaver", "class_tanks_1_1_data_1_1_json_saver.html", [
        [ "Tanks.Data.EncryptedJsonSaver", "class_tanks_1_1_data_1_1_encrypted_json_saver.html", null ]
      ] ]
    ] ],
    [ "IDragHandler", null, [
      [ "Tanks.UI.TankDragPreview", "class_tanks_1_1_u_i_1_1_tank_drag_preview.html", null ]
    ] ],
    [ "IEndDragHandler", null, [
      [ "Tanks.UI.TankDragPreview", "class_tanks_1_1_u_i_1_1_tank_drag_preview.html", null ]
    ] ],
    [ "Tanks.Extensions.IListExtensions", "class_tanks_1_1_extensions_1_1_i_list_extensions.html", null ],
    [ "ISerializationCallbackReceiver", null, [
      [ "Tanks.Data.DataStore", "class_tanks_1_1_data_1_1_data_store.html", null ]
    ] ],
    [ "Tanks.Rules.SinglePlayer.Objectives.KillLimit< Npc >", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_kill_limit.html", [
      [ "Tanks.Rules.SinglePlayer.Objectives.NpcKillLimit", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_npc_kill_limit.html", null ]
    ] ],
    [ "Tanks.Rules.SinglePlayer.Objectives.KillLimit< TargetNpc >", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_kill_limit.html", [
      [ "Tanks.Rules.SinglePlayer.Objectives.TargetNpcKillLimit", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_target_npc_kill_limit.html", [
        [ "Tanks.Rules.SinglePlayer.Objectives.ChaseAndDestroy", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_chase_and_destroy.html", null ]
      ] ]
    ] ],
    [ "Tanks.UI.LeaderboardElement", "class_tanks_1_1_u_i_1_1_leaderboard_element.html", null ],
    [ "Tanks.Data.LevelData", "class_tanks_1_1_data_1_1_level_data.html", null ],
    [ "Tanks.Map.MapDetails", "class_tanks_1_1_map_1_1_map_details.html", [
      [ "Tanks.Map.SinglePlayerMapDetails", "class_tanks_1_1_map_1_1_single_player_map_details.html", null ]
    ] ],
    [ "Tanks.Map.MapListBase< MapDetails >", "class_tanks_1_1_map_1_1_map_list_base.html", [
      [ "Tanks.Map.MapList", "class_tanks_1_1_map_1_1_map_list.html", null ]
    ] ],
    [ "Tanks.Map.MapListBase< SinglePlayerMapDetails >", "class_tanks_1_1_map_1_1_map_list_base.html", [
      [ "Tanks.Map.SinglePlayerMapList", "class_tanks_1_1_map_1_1_single_player_map_list.html", null ]
    ] ],
    [ "Tanks.Utilities.MathUtilities", "class_tanks_1_1_utilities_1_1_math_utilities.html", null ],
    [ "Tanks.Utilities.MobileUtilities", "class_tanks_1_1_utilities_1_1_mobile_utilities.html", null ],
    [ "Tanks.Rules.ModeDetails", "class_tanks_1_1_rules_1_1_mode_details.html", null ],
    [ "MonoBehaviour", null, [
      [ "Tanks.Audio.UIButtonAudio", "class_tanks_1_1_audio_1_1_u_i_button_audio.html", null ],
      [ "Tanks.CameraControl.CameraRigRotationInit", "class_tanks_1_1_camera_control_1_1_camera_rig_rotation_init.html", null ],
      [ "Tanks.CameraControl.CameraSnapshot", "class_tanks_1_1_camera_control_1_1_camera_snapshot.html", null ],
      [ "Tanks.Data.Decoration", "class_tanks_1_1_data_1_1_decoration.html", null ],
      [ "Tanks.Effects.DamageOutlineFlash", "class_tanks_1_1_effects_1_1_damage_outline_flash.html", null ],
      [ "Tanks.Effects.Effect", "class_tanks_1_1_effects_1_1_effect.html", null ],
      [ "Tanks.Effects.MineTimer", "class_tanks_1_1_effects_1_1_mine_timer.html", null ],
      [ "Tanks.Effects.PlumeSpawner", "class_tanks_1_1_effects_1_1_plume_spawner.html", null ],
      [ "Tanks.Hazards.Turret", "class_tanks_1_1_hazards_1_1_turret.html", null ],
      [ "Tanks.Managers.QualitySettingsManager", "class_tanks_1_1_managers_1_1_quality_settings_manager.html", null ],
      [ "Tanks.Map.SpawnPoint", "class_tanks_1_1_map_1_1_spawn_point.html", null ],
      [ "Tanks.RespawningTank", "class_tanks_1_1_respawning_tank.html", null ],
      [ "Tanks.Rules.RulesProcessor", "class_tanks_1_1_rules_1_1_rules_processor.html", [
        [ "Tanks.Rules.FreeForAll", "class_tanks_1_1_rules_1_1_free_for_all.html", null ],
        [ "Tanks.Rules.LastManStanding", "class_tanks_1_1_rules_1_1_last_man_standing.html", null ],
        [ "Tanks.Rules.SinglePlayer.OfflineRulesProcessor", "class_tanks_1_1_rules_1_1_single_player_1_1_offline_rules_processor.html", [
          [ "Tanks.Rules.SinglePlayer.ShootingRangeRulesProcessor", "class_tanks_1_1_rules_1_1_single_player_1_1_shooting_range_rules_processor.html", null ],
          [ "Tanks.Rules.SinglePlayer.SinglePlayerRulesProcessor", "class_tanks_1_1_rules_1_1_single_player_1_1_single_player_rules_processor.html", null ]
        ] ],
        [ "Tanks.Rules.TeamDeathmatch", "class_tanks_1_1_rules_1_1_team_deathmatch.html", null ]
      ] ],
      [ "Tanks.Rules.SinglePlayer.Navigator", "class_tanks_1_1_rules_1_1_single_player_1_1_navigator.html", null ],
      [ "Tanks.Rules.SinglePlayer.Objectives.Objective", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_objective.html", [
        [ "Tanks.Rules.SinglePlayer.Objectives.Collection", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_collection.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.DontTakeDamage", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_dont_take_damage.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.Escort", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_escort.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.EscortUntilTargetDies", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_escort_until_target_dies.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.GetToLocation", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_get_to_location.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.KillLimit< T >", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_kill_limit.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.KillTarget", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_kill_target.html", [
          [ "Tanks.Rules.SinglePlayer.Objectives.Chase", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_chase.html", null ]
        ] ],
        [ "Tanks.Rules.SinglePlayer.Objectives.TimedFailure", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_timed_failure.html", null ],
        [ "Tanks.Rules.SinglePlayer.Objectives.TimedSuccess", "class_tanks_1_1_rules_1_1_single_player_1_1_objectives_1_1_timed_success.html", null ]
      ] ],
      [ "Tanks.Rules.SinglePlayer.Waypoint", "class_tanks_1_1_rules_1_1_single_player_1_1_waypoint.html", null ],
      [ "Tanks.Shells.PhysicsAffected", "class_tanks_1_1_shells_1_1_physics_affected.html", null ],
      [ "Tanks.Shells.Shell", "class_tanks_1_1_shells_1_1_shell.html", null ],
      [ "Tanks.SinglePlayer.Npc", "class_tanks_1_1_single_player_1_1_npc.html", null ],
      [ "Tanks.SinglePlayer.ObjectiveArrowBehaviour", "class_tanks_1_1_single_player_1_1_objective_arrow_behaviour.html", null ],
      [ "Tanks.SinglePlayer.TargetZone", "class_tanks_1_1_single_player_1_1_target_zone.html", [
        [ "Tanks.SinglePlayer.Collectible", "class_tanks_1_1_single_player_1_1_collectible.html", null ]
      ] ],
      [ "Tanks.SpringDetach", "class_tanks_1_1_spring_detach.html", null ],
      [ "Tanks.TankControllers.TankDisplay", "class_tanks_1_1_tank_controllers_1_1_tank_display.html", null ],
      [ "Tanks.UI.BackButton", "class_tanks_1_1_u_i_1_1_back_button.html", [
        [ "Tanks.UI.AndroidBackQuit", "class_tanks_1_1_u_i_1_1_android_back_quit.html", null ],
        [ "Tanks.UI.BackButtonBlocker", "class_tanks_1_1_u_i_1_1_back_button_blocker.html", null ]
      ] ],
      [ "Tanks.UI.CreateGame", "class_tanks_1_1_u_i_1_1_create_game.html", null ],
      [ "Tanks.UI.CurrencyPanel", "class_tanks_1_1_u_i_1_1_currency_panel.html", null ],
      [ "Tanks.UI.DiscretePointSlider", "class_tanks_1_1_u_i_1_1_discrete_point_slider.html", null ],
      [ "Tanks.UI.EndGameCountDown", "class_tanks_1_1_u_i_1_1_end_game_count_down.html", null ],
      [ "Tanks.UI.FadingGroup", "class_tanks_1_1_u_i_1_1_fading_group.html", null ],
      [ "Tanks.UI.HUDMultiplayerScore", "class_tanks_1_1_u_i_1_1_h_u_d_multiplayer_score.html", null ],
      [ "Tanks.UI.HUDSinglePlayer", "class_tanks_1_1_u_i_1_1_h_u_d_single_player.html", null ],
      [ "Tanks.UI.InGameNotification", "class_tanks_1_1_u_i_1_1_in_game_notification.html", null ],
      [ "Tanks.UI.InstructionSwap", "class_tanks_1_1_u_i_1_1_instruction_swap.html", null ],
      [ "Tanks.UI.LabelScaling", "class_tanks_1_1_u_i_1_1_label_scaling.html", null ],
      [ "Tanks.UI.LeaderboardUI", "class_tanks_1_1_u_i_1_1_leaderboard_u_i.html", null ],
      [ "Tanks.UI.LeaderboardUIElement", "class_tanks_1_1_u_i_1_1_leaderboard_u_i_element.html", null ],
      [ "Tanks.UI.LobbyInfoPanel", "class_tanks_1_1_u_i_1_1_lobby_info_panel.html", null ],
      [ "Tanks.UI.LobbyPanel", "class_tanks_1_1_u_i_1_1_lobby_panel.html", null ],
      [ "Tanks.UI.LobbyPlayer", "class_tanks_1_1_u_i_1_1_lobby_player.html", null ],
      [ "Tanks.UI.LobbyPlayerList", "class_tanks_1_1_u_i_1_1_lobby_player_list.html", null ],
      [ "Tanks.UI.LobbyServerEntry", "class_tanks_1_1_u_i_1_1_lobby_server_entry.html", null ],
      [ "Tanks.UI.LobbyServerList", "class_tanks_1_1_u_i_1_1_lobby_server_list.html", null ],
      [ "Tanks.UI.Modal", "class_tanks_1_1_u_i_1_1_modal.html", [
        [ "Tanks.UI.AdvertUnlockModal", "class_tanks_1_1_u_i_1_1_advert_unlock_modal.html", null ],
        [ "Tanks.UI.BuyModal", "class_tanks_1_1_u_i_1_1_buy_modal.html", null ],
        [ "Tanks.UI.EndGameModal", "class_tanks_1_1_u_i_1_1_end_game_modal.html", [
          [ "Tanks.UI.MultiplayerEndGameModal", "class_tanks_1_1_u_i_1_1_multiplayer_end_game_modal.html", null ],
          [ "Tanks.UI.OfflineEndGameModal", "class_tanks_1_1_u_i_1_1_offline_end_game_modal.html", [
            [ "Tanks.UI.ShootingRangeEndGameModal", "class_tanks_1_1_u_i_1_1_shooting_range_end_game_modal.html", null ],
            [ "Tanks.UI.SinglePlayerCompleteModal", "class_tanks_1_1_u_i_1_1_single_player_complete_modal.html", null ]
          ] ]
        ] ],
        [ "Tanks.UI.LoadingModal", "class_tanks_1_1_u_i_1_1_loading_modal.html", null ],
        [ "Tanks.UI.RouletteModal", "class_tanks_1_1_u_i_1_1_roulette_modal.html", null ],
        [ "Tanks.UI.SettingsModal", "class_tanks_1_1_u_i_1_1_settings_modal.html", null ],
        [ "Tanks.UI.SharingModal", "class_tanks_1_1_u_i_1_1_sharing_modal.html", null ],
        [ "Tanks.UI.ShopScreen", "class_tanks_1_1_u_i_1_1_shop_screen.html", null ],
        [ "Tanks.UI.StartGameModal", "class_tanks_1_1_u_i_1_1_start_game_modal.html", [
          [ "Tanks.UI.SinglePlayerStartGameModal", "class_tanks_1_1_u_i_1_1_single_player_start_game_modal.html", null ]
        ] ],
        [ "Tanks.UI.TimedModal", "class_tanks_1_1_u_i_1_1_timed_modal.html", null ]
      ] ],
      [ "Tanks.UI.NamePanel", "class_tanks_1_1_u_i_1_1_name_panel.html", null ],
      [ "Tanks.UI.NumberDisplay", "class_tanks_1_1_u_i_1_1_number_display.html", null ],
      [ "Tanks.UI.ObjectiveUI", "class_tanks_1_1_u_i_1_1_objective_u_i.html", null ],
      [ "Tanks.UI.PauseButton", "class_tanks_1_1_u_i_1_1_pause_button.html", null ],
      [ "Tanks.UI.PlatformSpecificText", "class_tanks_1_1_u_i_1_1_platform_specific_text.html", null ],
      [ "Tanks.UI.PulsingUIGroup", "class_tanks_1_1_u_i_1_1_pulsing_u_i_group.html", [
        [ "Tanks.UI.PulsingInputBox", "class_tanks_1_1_u_i_1_1_pulsing_input_box.html", null ]
      ] ],
      [ "Tanks.UI.RenderTextureConsumer", "class_tanks_1_1_u_i_1_1_render_texture_consumer.html", null ],
      [ "Tanks.UI.Select", "class_tanks_1_1_u_i_1_1_select.html", [
        [ "Tanks.UI.LevelSelect", "class_tanks_1_1_u_i_1_1_level_select.html", null ],
        [ "Tanks.UI.LobbyGameDetails", "class_tanks_1_1_u_i_1_1_lobby_game_details.html", null ],
        [ "Tanks.UI.MapSelect", "class_tanks_1_1_u_i_1_1_map_select.html", null ],
        [ "Tanks.UI.ModeSelect", "class_tanks_1_1_u_i_1_1_mode_select.html", null ]
      ] ],
      [ "Tanks.UI.SimpleSplashScreen", "class_tanks_1_1_u_i_1_1_simple_splash_screen.html", null ],
      [ "Tanks.UI.SinglePlayerCompleteModalAchievement", "class_tanks_1_1_u_i_1_1_single_player_complete_modal_achievement.html", null ],
      [ "Tanks.UI.Skin", "class_tanks_1_1_u_i_1_1_skin.html", null ],
      [ "Tanks.UI.SkinColour", "class_tanks_1_1_u_i_1_1_skin_colour.html", null ],
      [ "Tanks.UI.SkinColourSelector", "class_tanks_1_1_u_i_1_1_skin_colour_selector.html", null ],
      [ "Tanks.UI.SkinSelect", "class_tanks_1_1_u_i_1_1_skin_select.html", null ],
      [ "Tanks.UI.TankDragPreview", "class_tanks_1_1_u_i_1_1_tank_drag_preview.html", null ],
      [ "Tanks.UI.TankSelector", "class_tanks_1_1_u_i_1_1_tank_selector.html", [
        [ "Tanks.UI.LobbyCustomization", "class_tanks_1_1_u_i_1_1_lobby_customization.html", null ],
        [ "Tanks.UI.TankCustomizationModal", "class_tanks_1_1_u_i_1_1_tank_customization_modal.html", null ]
      ] ],
      [ "Tanks.UI.ThumbnailAnimator", "class_tanks_1_1_u_i_1_1_thumbnail_animator.html", null ],
      [ "Tanks.UI.UIDirectionControl", "class_tanks_1_1_u_i_1_1_u_i_direction_control.html", null ],
      [ "Tanks.UI.UiShake", "class_tanks_1_1_u_i_1_1_ui_shake.html", null ],
      [ "Tanks.Utilities.MobileDisable", "class_tanks_1_1_utilities_1_1_mobile_disable.html", null ],
      [ "Tanks.Utilities.Singleton< T >", "class_tanks_1_1_utilities_1_1_singleton.html", [
        [ "Tanks.Utilities.PersistentSingleton< T >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", null ]
      ] ]
    ] ],
    [ "NetworkBehaviour", null, [
      [ "Tanks.Explosions.ExplosionManager", "class_tanks_1_1_explosions_1_1_explosion_manager.html", null ],
      [ "Tanks.FX.HotdropLight", "class_tanks_1_1_f_x_1_1_hotdrop_light.html", null ],
      [ "Tanks.GameManager", "class_tanks_1_1_game_manager.html", null ],
      [ "Tanks.Hazards.LevelHazard", "class_tanks_1_1_hazards_1_1_level_hazard.html", [
        [ "Tanks.Hazards.MineController", "class_tanks_1_1_hazards_1_1_mine_controller.html", null ]
      ] ],
      [ "Tanks.Networking.NetworkPlayer", "class_tanks_1_1_networking_1_1_network_player.html", null ],
      [ "Tanks.Pickups.CrateSpawner", "class_tanks_1_1_pickups_1_1_crate_spawner.html", null ],
      [ "Tanks.Pickups.PickupBase", "class_tanks_1_1_pickups_1_1_pickup_base.html", null ],
      [ "Tanks.Pickups.TankSeeker", "class_tanks_1_1_pickups_1_1_tank_seeker.html", null ],
      [ "Tanks.TankControllers.TankHealth", "class_tanks_1_1_tank_controllers_1_1_tank_health.html", null ],
      [ "Tanks.TankControllers.TankInputModule", "class_tanks_1_1_tank_controllers_1_1_tank_input_module.html", [
        [ "Tanks.TankControllers.TankKeyboardInput", "class_tanks_1_1_tank_controllers_1_1_tank_keyboard_input.html", null ],
        [ "Tanks.TankControllers.TankTouchInput", "class_tanks_1_1_tank_controllers_1_1_tank_touch_input.html", null ]
      ] ],
      [ "Tanks.TankControllers.TankManager", "class_tanks_1_1_tank_controllers_1_1_tank_manager.html", null ],
      [ "Tanks.TankControllers.TankMovement", "class_tanks_1_1_tank_controllers_1_1_tank_movement.html", null ],
      [ "Tanks.TankControllers.TankShooting", "class_tanks_1_1_tank_controllers_1_1_tank_shooting.html", null ]
    ] ],
    [ "NetworkManager", null, [
      [ "Tanks.Networking.NetworkManager", "class_tanks_1_1_networking_1_1_network_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< GameSettings >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.GameSettings", "class_tanks_1_1_game_settings.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< MusicManager >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Audio.MusicManager", "class_tanks_1_1_audio_1_1_music_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< SpecialProjectileLibrary >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Data.SpecialProjectileLibrary", "class_tanks_1_1_data_1_1_special_projectile_library.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< TankDecorationLibrary >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Data.TankDecorationLibrary", "class_tanks_1_1_data_1_1_tank_decoration_library.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< TankLibrary >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Data.TankLibrary", "class_tanks_1_1_data_1_1_tank_library.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< TanksAdvertController >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Advertising.TanksAdvertController", "class_tanks_1_1_advertising_1_1_tanks_advert_controller.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< ThemedEffectsLibrary >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Effects.ThemedEffectsLibrary", "class_tanks_1_1_effects_1_1_themed_effects_library.html", null ]
    ] ],
    [ "Tanks.Utilities.PersistentSingleton< UIAudioManager >", "class_tanks_1_1_utilities_1_1_persistent_singleton.html", [
      [ "Tanks.Audio.UIAudioManager", "class_tanks_1_1_audio_1_1_u_i_audio_manager.html", null ]
    ] ],
    [ "Tanks.Pickups.PowerupDefinition", "struct_tanks_1_1_pickups_1_1_powerup_definition.html", null ],
    [ "Tanks.Data.ProjectileDefinition", "struct_tanks_1_1_data_1_1_projectile_definition.html", null ],
    [ "ScriptableObject", null, [
      [ "Tanks.Explosions.DebrisSettings", "class_tanks_1_1_explosions_1_1_debris_settings.html", null ],
      [ "Tanks.Explosions.ExplosionSettings", "class_tanks_1_1_explosions_1_1_explosion_settings.html", null ],
      [ "Tanks.IAP.IAPContent", "class_tanks_1_1_i_a_p_1_1_i_a_p_content.html", [
        [ "Tanks.IAP.CurrencyContent", "class_tanks_1_1_i_a_p_1_1_currency_content.html", null ]
      ] ],
      [ "Tanks.Map.MapListBase< T >", "class_tanks_1_1_map_1_1_map_list_base.html", null ],
      [ "Tanks.Rules.KillLogPhrases", "class_tanks_1_1_rules_1_1_kill_log_phrases.html", null ],
      [ "Tanks.Rules.ModeList", "class_tanks_1_1_rules_1_1_mode_list.html", null ]
    ] ],
    [ "Tanks.Data.SettingsData", "class_tanks_1_1_data_1_1_settings_data.html", null ],
    [ "Tanks.CameraControl.ScreenShakeController.ShakeInstance", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_instance.html", null ],
    [ "Tanks.CameraControl.ScreenShakeController.ShakeSettings", "struct_tanks_1_1_camera_control_1_1_screen_shake_controller_1_1_shake_settings.html", null ],
    [ "Tanks.Utilities.Singleton< AnnouncerModal >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.AnnouncerModal", "class_tanks_1_1_u_i_1_1_announcer_modal.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< CameraFollow >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.CameraControl.CameraFollow", "class_tanks_1_1_camera_control_1_1_camera_follow.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< CrateManager >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.SinglePlayer.CrateManager", "class_tanks_1_1_single_player_1_1_crate_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< DailyUnlockManager >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.Data.DailyUnlockManager", "class_tanks_1_1_data_1_1_daily_unlock_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< HUDController >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.HUDController", "class_tanks_1_1_u_i_1_1_h_u_d_controller.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< InGameLeaderboardModal >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.InGameLeaderboardModal", "class_tanks_1_1_u_i_1_1_in_game_leaderboard_modal.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< InGameNotificationManager >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.InGameNotificationManager", "class_tanks_1_1_u_i_1_1_in_game_notification_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< InGameOptionsMenu >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.InGameOptionsMenu", "class_tanks_1_1_u_i_1_1_in_game_options_menu.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< MainMenuUI >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.MainMenuUI", "class_tanks_1_1_u_i_1_1_main_menu_u_i.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< PlayerDataManager >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.Data.PlayerDataManager", "class_tanks_1_1_data_1_1_player_data_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< ScreenShakeController >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.CameraControl.ScreenShakeController", "class_tanks_1_1_camera_control_1_1_screen_shake_controller.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< SpawnManager >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.Map.SpawnManager", "class_tanks_1_1_map_1_1_spawn_manager.html", null ]
    ] ],
    [ "Tanks.Utilities.Singleton< TankRotator >", "class_tanks_1_1_utilities_1_1_singleton.html", [
      [ "Tanks.UI.TankRotator", "class_tanks_1_1_u_i_1_1_tank_rotator.html", null ]
    ] ],
    [ "Tanks.Utilities.StringBuilding", "class_tanks_1_1_utilities_1_1_string_building.html", null ],
    [ "Tanks.Data.TankDecorationDefinition", "struct_tanks_1_1_data_1_1_tank_decoration_definition.html", null ],
    [ "Tanks.Data.TankTypeDefinition", "struct_tanks_1_1_data_1_1_tank_type_definition.html", null ],
    [ "Tanks.Rules.Team", "class_tanks_1_1_rules_1_1_team.html", null ],
    [ "Tanks.Vector2Extensions", "class_tanks_1_1_vector2_extensions.html", null ]
];