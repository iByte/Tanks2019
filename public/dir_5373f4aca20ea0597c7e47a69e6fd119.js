var dir_5373f4aca20ea0597c7e47a69e6fd119 =
[
    [ "MapDetails.cs", "_map_details_8cs.html", [
      [ "MapDetails", "class_tanks_1_1_map_1_1_map_details.html", "class_tanks_1_1_map_1_1_map_details" ]
    ] ],
    [ "MapEffectsGroup.cs", "_map_effects_group_8cs.html", "_map_effects_group_8cs" ],
    [ "MapList.cs", "_map_list_8cs.html", [
      [ "MapList", "class_tanks_1_1_map_1_1_map_list.html", null ]
    ] ],
    [ "MapListBase.cs", "_map_list_base_8cs.html", [
      [ "MapListBase", "class_tanks_1_1_map_1_1_map_list_base.html", "class_tanks_1_1_map_1_1_map_list_base" ]
    ] ],
    [ "SinglePlayerMapDetails.cs", "_single_player_map_details_8cs.html", [
      [ "SinglePlayerMapDetails", "class_tanks_1_1_map_1_1_single_player_map_details.html", "class_tanks_1_1_map_1_1_single_player_map_details" ]
    ] ],
    [ "SinglePlayerMapList.cs", "_single_player_map_list_8cs.html", [
      [ "SinglePlayerMapList", "class_tanks_1_1_map_1_1_single_player_map_list.html", null ]
    ] ],
    [ "SpawnManager.cs", "_spawn_manager_8cs.html", [
      [ "SpawnManager", "class_tanks_1_1_map_1_1_spawn_manager.html", "class_tanks_1_1_map_1_1_spawn_manager" ]
    ] ],
    [ "SpawnPoint.cs", "_spawn_point_8cs.html", [
      [ "SpawnPoint", "class_tanks_1_1_map_1_1_spawn_point.html", "class_tanks_1_1_map_1_1_spawn_point" ]
    ] ]
];