var class_tanks_1_1_rules_1_1_kill_log_phrases =
[
    [ "GetRandomKillPhrase", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#ac063d44d5db47358e087f52468fd621a", null ],
    [ "GetRandomKillPhraseSyntax", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#aba9ae1f5635f7ba59adcb04c1bbe7e76", null ],
    [ "GetRandomSuicidePhrase", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#a6c1449d125232c9a8811f484161afc68", null ],
    [ "GetRandomSuicidePhraseSyntax", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#ae0cd92ed0402e93fc6916c720924175f", null ],
    [ "m_KillPhrases", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#ac6e1a15da61189d9b3802f7642bd8cba", null ],
    [ "m_SuicidePhrases", "class_tanks_1_1_rules_1_1_kill_log_phrases.html#aca0e6459f3d73f698ac9c241ce604ac4", null ]
];