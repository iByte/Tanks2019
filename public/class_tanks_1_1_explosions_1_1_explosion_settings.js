var class_tanks_1_1_explosions_1_1_explosion_settings =
[
    [ "damage", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a90a05a17413194587f9995394cc86ecd", null ],
    [ "explosionClass", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a0338ca6f50b857566a1eedca20deb230", null ],
    [ "explosionRadius", "class_tanks_1_1_explosions_1_1_explosion_settings.html#add876542b3e1a83a776dd6b81d803202", null ],
    [ "id", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a7c26b6acf0e88ff81799575f1156c8c6", null ],
    [ "physicsForce", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a4d8a6a12d7149f09dcefe1d6a9a5d73a", null ],
    [ "physicsRadius", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a195143a9ef04b01cdbf926fb46ff96f1", null ],
    [ "shakeMagnitude", "class_tanks_1_1_explosions_1_1_explosion_settings.html#a8c006ce8211d665adc36e740c1396dc5", null ]
];