var class_tanks_1_1_data_1_1_special_projectile_library =
[
    [ "Awake", "class_tanks_1_1_data_1_1_special_projectile_library.html#a73cac1ab8b1171b38826aa7cd64fdd96", null ],
    [ "GetFireSoundForIndex", "class_tanks_1_1_data_1_1_special_projectile_library.html#afbeddf423f92a6e45b567f04223d0af7", null ],
    [ "GetNumberOfDefinitions", "class_tanks_1_1_data_1_1_special_projectile_library.html#a1903239722bf06e3915f6a7d5e0ac489", null ],
    [ "GetProjectileDataForIndex", "class_tanks_1_1_data_1_1_special_projectile_library.html#a7bf8ca3ba4d291cdfbb15138cbc84872", null ],
    [ "shellDefinitions", "class_tanks_1_1_data_1_1_special_projectile_library.html#a41a4303f8c8bad09ed748b3b76f00745", null ]
];