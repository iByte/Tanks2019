var dir_54917bde386a5ef9e0c3f63ca6b256ef =
[
    [ "ExplosionManager.cs", "_explosion_manager_8cs.html", [
      [ "ExplosionManager", "class_tanks_1_1_explosions_1_1_explosion_manager.html", "class_tanks_1_1_explosions_1_1_explosion_manager" ]
    ] ],
    [ "GameManager.cs", "_game_manager_8cs.html", "_game_manager_8cs" ],
    [ "GameSettings.cs", "_game_settings_8cs.html", [
      [ "GameSettings", "class_tanks_1_1_game_settings.html", "class_tanks_1_1_game_settings" ]
    ] ],
    [ "NetworkManager.cs", "_network_manager_8cs.html", "_network_manager_8cs" ],
    [ "QualitySettingsManager.cs", "_quality_settings_manager_8cs.html", [
      [ "QualitySettingsManager", "class_tanks_1_1_managers_1_1_quality_settings_manager.html", "class_tanks_1_1_managers_1_1_quality_settings_manager" ]
    ] ],
    [ "RespawningTank.cs", "_respawning_tank_8cs.html", [
      [ "RespawningTank", "class_tanks_1_1_respawning_tank.html", "class_tanks_1_1_respawning_tank" ]
    ] ],
    [ "TankManager.cs", "_tank_manager_8cs.html", "_tank_manager_8cs" ]
];