var class_tanks_1_1_analytics_1_1_heatmaps_helper =
[
    [ "LogHeatmapEvent", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html#a23011dbf89ba65be477e1d3124e7af32", null ],
    [ "MultiplayerDeath", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html#af126c023a328c2ae5219093fdacfaace", null ],
    [ "MultiplayerKill", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html#a8d9baccb42a4f2189d3b5c2ff930a262", null ],
    [ "MultiplayerSuicide", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html#a6ec080475c69c3ebb9b5aaa6b465d682", null ],
    [ "SinglePlayerDeath", "class_tanks_1_1_analytics_1_1_heatmaps_helper.html#a97c098a88e9de0917d3741a784547174", null ]
];