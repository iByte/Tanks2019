var namespace_tanks_1_1_camera_control =
[
    [ "CameraFollow", "class_tanks_1_1_camera_control_1_1_camera_follow.html", "class_tanks_1_1_camera_control_1_1_camera_follow" ],
    [ "CameraRigRotationInit", "class_tanks_1_1_camera_control_1_1_camera_rig_rotation_init.html", "class_tanks_1_1_camera_control_1_1_camera_rig_rotation_init" ],
    [ "CameraSnapshot", "class_tanks_1_1_camera_control_1_1_camera_snapshot.html", "class_tanks_1_1_camera_control_1_1_camera_snapshot" ],
    [ "ScreenShakeController", "class_tanks_1_1_camera_control_1_1_screen_shake_controller.html", "class_tanks_1_1_camera_control_1_1_screen_shake_controller" ]
];